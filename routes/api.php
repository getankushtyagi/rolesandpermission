<?php

use App\Http\Controllers\UserController;
use App\Http\Middleware\UserMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function () {


});


Route::post('login', [UserController::class, 'loginuser']);
Route::post('register', [UserController::class, 'registration']);

Route::group(['middleware'=>['auth:sanctum']], function () {

    Route::post('blog', [UserController::class, 'addblog']);
    Route::post('createmanager', [UserController::class, 'adminCreateManager']);
    Route::post('showblog', [UserController::class, 'showAllBlog']);
    Route::post('logout', [UserController::class, 'logoutuser']);

    Route::post('/roles/{role}/assign/{user}', [UserController::class,'rolesAddUser']);

});

