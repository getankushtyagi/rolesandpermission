<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller{

    public function registration(Request $request){

        $input = $request->all();
        $input['name'] = $request->name;
        $input['email'] = $request->email;
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken("token")->plainTextToken;
        // $input['remember_token'] = $user->createToken("token")-> plainTextToken;

        $success['name'] =  $user->name;
        return response()->json(['success' => $success]);
    }

    public function loginuser(Request $request){
        try {

            $user = User::where('email', $request->email)->first();
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    $token = $user->createToken('token')->plainTextToken;
                    $response = ['token' => $token];
                    return response($response, 422);
                } else {
                    $response = ["Incorrect" => 'Credentials does not match '];
                    return response($response, 422);
                }
            } else
                return ["sorry" => "email does not exist"];
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function addblog(Request $request){

        {
            try {
                $user_id = $request->user()->role_id;
                //  Auth::user()->role_id;

                if($user_id=='1'){

                    $input = $request->all();
                    $blog = Blog::create($input);
                    $success['token'] =  $blog->createToken('App')-> plainTextToken;
                    return response()->json(['success'=>$success]);
                }

                else if($user_id=='2'){

                    $input = $request->all();
                    $blog = Blog::create($input);
                    $success['token'] =  $blog->createToken('App')-> plainTextToken;
                    return response()->json(['success'=>$success]);
                }

                else{

                $input = $request->all();
                $blog = Blog::create($input);
                $success['token'] =  $blog->createToken('App')-> plainTextToken;
                return response()->json(['success'=>$success]);
                }
            }

            catch (\Exception $e) {
                dd($e);
            }
        }
    }

    public function adminCreateManager(Request $request){


        $user_role_id = Auth::user()->role_id;

        // dd($user_role_id);

        if ($user_role_id == 1) {

            $input = $request->all();
            $input['name'] = $request->name;
            $input['email'] = $request->email;
            $input['password'] = bcrypt($input['password']);
            $input['role_id'] = $request->role_id;

            $user = User::create($input);
            $success['token'] =  $user->createToken("token")->plainTextToken;
            // $input['remember_token'] = $user->createToken("token")-> plainTextToken;

            $success['name'] =  $user->name;
            return response()->json(['success' => $success]);
        } else {
            return ["result" => "you are not admin"];
        }
    }

    public function logoutuser(Request $request){
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

    public function showAllBlog(Request $request){
        try {
            $user_role_id = Auth::user();
            // dd($user_role_id);

            if ($user_role_id == '1') {
                $arr = User::with('blog')->get();
                return response()->json(['success' => $arr]);
            } else if ($user_role_id == '2') {
                $arr = User::with('blog')->get();
                return response()->json(['success' => $arr]);
            } else {
                $user_id = $request->user()->id;
                $arr = User::with('blog')->where('id', $user_id)->get();
                return response()->json(['success' => $arr]);
            }
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function rolesAddUser(Request $request, User $role_id, Permission $permission_id){

        // $permission_id->assignRole($role_id);
        $role_id->givePermissionTo($permission_id);
        return response()->json([
            "message" => $role_id->name . " Role successfully assigned to User!"
        ], 200);
    }

}
